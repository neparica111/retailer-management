/**
 * Created by shital on 9/30/16.
 */
var xmlhttp = new XMLHttpRequest();
var url = "json/blog.json";
var data = "";
xmlhttp.onreadystatechange = function (){
    if (this.readyState == 4 && this.status == 200) {
        var myArr = JSON.parse(this.responseText);
        myfunction(myArr);
        data = myArr;
    }
};

xmlhttp.open("GET", url, true);
xmlhttp.send();

blogbody = document.getElementById("blogBody");
elements = "";
function myfunction(arr){
    var i;
    var j = 0;
    j = arr.length;
    j = (j/2 ) - (j/2 * 10 % 10 /10 );
    for(i=0;i<j;i++){
        var title1 = arr[i].title;
        var url1 = arr[i].url;
        var created = arr[i].created_at;
        var excerpt = arr[i].excerpt;
        var add_title = "";
        add_title = '<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 rem1" ><div class="post-meta"><time itemprop="datePublished">'+created+'</time> </div>';
        add_title += '<a itemprop="url" href="'+url1+'">';
        add_title += '<h2 class="post-title">'+title1+'</h2>';
        add_title += '<p class="post-subtitle">'+excerpt+'</p></a><hr class="blog-hr"></div>';
        $("div#blogBody").append(add_title);
    }
}
$(document).ready(function () {
    $(".next").click(function () {
        $(".rem1").remove();
        $(".rem2").remove();
        var i = 0;
        i = data.length;
        i = (i/2 ) - (i/2 * 10 % 10 /10 );
        for(i;i<data.length;i++){
            var title1 = data[i].title;
            var url1 = data[i].url;
            var created = data[i].created_at;
            var excerpt = data[i].excerpt;
            var add_title = "";
            add_title = '<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 rem2" ><div class="post-meta"><time itemprop="datePublished">'+created+'</time> </div>';
            add_title += '<a itemprop="url" class="title" href="'+url1+'">';
            add_title += '<h2 class="post-title">'+title1+'</h2>';
            add_title += '<p class="post-subtitle">'+excerpt+'</p></a><hr class="blog-hr"></div>';
            $("div#blogBody").append(add_title);
        }

    });
});

$(document).ready(function(){
    $(".previous").click(function(){
        $(".rem2").remove();
        $(".rem1").remove();
        myfunction(data);
    });
});